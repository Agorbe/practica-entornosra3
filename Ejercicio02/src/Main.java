import java.io.*;
import java.util.*;
/**
 * 
 * @author Arturo Gorbe
 * @since 31-01-2018
 */
public class Main {
	public static void main(String[] args) 
	{
		//se cambia el nombre de la variable ya que a va a ser usada mas adelante
		Scanner teclado = new Scanner(System.in);
		int cantidad_maxima_alumnos = 10;
		int alumnos[] = new int[10];
		for(int n=0;n<10;n++) //se declara n dentro del for para acortar
		{
			System.out.println("Introduce nota media de alumno");
			alumnos[n] = teclado.nextInt();
		}	
		
		System.out.println("El resultado es: " + mostrar_alumnos(alumnos));
		
		teclado.close();
	}
	static double mostrar_alumnos(int vector[])
	{
		double c = 0;
		for(int a=0;a<10;a++) 
		{
			c+=vector[a]; //con esto(c+=) acortamos la sintaxis
		}
		return (c/10); //ponemos parentesis
	}
	
}